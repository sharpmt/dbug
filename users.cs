﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DBug
{
    public class Users
    {
        public int User_id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Role { get; set; }
        public string PAT { get; set; }
        private SqlConnection connection;
        private Users user;

        public Users()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionSrc"].ConnectionString);
        }

        public Users GetUserRecord(string given_username)
        {
            user = new Users();
            // Using stored procedure to view data
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            SqlCommand cmd = new SqlCommand("AccountSelect", connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@username", given_username);

            SqlDataReader record = cmd.ExecuteReader();
            record.Read();

            if (!(record.HasRows))
            {
                throw new UnauthorizedAccessException("User " + given_username + " does not exist.");
            }
            else
            {
                if (!(record["password"].ToString().Equals(DBNull.Value)))
                {
                    user.Username = record["username"].ToString();
                    user.User_id = Convert.ToInt32(record["user_id"].ToString());
                    user.Password = record["password"].ToString();
                    user.PAT = record["pat"].ToString();
                    user.Role = Convert.ToInt32(record["role"].ToString());
                    return user;
                }
            }
            return null;
        }
    }
}
