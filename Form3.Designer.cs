﻿namespace DBug
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.auditsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersDataSet1 = new DBug.usersDataSet1();
            this.auditsTableAdapter = new DBug.usersDataSet1TableAdapters.auditsTableAdapter();
            this.bug_id_textbox = new System.Windows.Forms.TextBox();
            this.audit_id_textbox = new System.Windows.Forms.TextBox();
            this.audit_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bug_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.a_comments = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.a_timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.a_update = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.auditsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.audit_id,
            this.bug_id,
            this.a_comments,
            this.a_timestamp,
            this.a_update});
            this.dataGridView1.DataSource = this.auditsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(644, 376);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // auditsBindingSource
            // 
            this.auditsBindingSource.DataMember = "audits";
            this.auditsBindingSource.DataSource = this.usersDataSet1;
            // 
            // usersDataSet1
            // 
            this.usersDataSet1.DataSetName = "usersDataSet1";
            this.usersDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // auditsTableAdapter
            // 
            this.auditsTableAdapter.ClearBeforeFill = true;
            // 
            // bug_id_textbox
            // 
            this.bug_id_textbox.Location = new System.Drawing.Point(532, 342);
            this.bug_id_textbox.Name = "bug_id_textbox";
            this.bug_id_textbox.Size = new System.Drawing.Size(100, 20);
            this.bug_id_textbox.TabIndex = 1;
            this.bug_id_textbox.Visible = false;
            // 
            // audit_id_textbox
            // 
            this.audit_id_textbox.Location = new System.Drawing.Point(532, 316);
            this.audit_id_textbox.Name = "audit_id_textbox";
            this.audit_id_textbox.Size = new System.Drawing.Size(100, 20);
            this.audit_id_textbox.TabIndex = 2;
            this.audit_id_textbox.Visible = false;
            // 
            // audit_id
            // 
            this.audit_id.DataPropertyName = "audit_id";
            this.audit_id.HeaderText = "Audit ID";
            this.audit_id.Name = "audit_id";
            this.audit_id.ReadOnly = true;
            this.audit_id.Width = 75;
            // 
            // bug_id
            // 
            this.bug_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.bug_id.DataPropertyName = "bug_id";
            this.bug_id.HeaderText = "Bug ID";
            this.bug_id.Name = "bug_id";
            this.bug_id.ReadOnly = true;
            this.bug_id.Width = 50;
            // 
            // a_comments
            // 
            this.a_comments.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.a_comments.DataPropertyName = "a_comments";
            this.a_comments.HeaderText = "Comments";
            this.a_comments.Name = "a_comments";
            this.a_comments.ReadOnly = true;
            // 
            // a_timestamp
            // 
            this.a_timestamp.DataPropertyName = "a_timestamp";
            this.a_timestamp.HeaderText = "Timestamp";
            this.a_timestamp.Name = "a_timestamp";
            this.a_timestamp.ReadOnly = true;
            this.a_timestamp.Width = 150;
            // 
            // a_update
            // 
            this.a_update.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.a_update.DataPropertyName = "a_update";
            this.a_update.HeaderText = "Update";
            this.a_update.Name = "a_update";
            this.a_update.ReadOnly = true;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 374);
            this.Controls.Add(this.audit_id_textbox);
            this.Controls.Add(this.bug_id_textbox);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Audit log";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.auditsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private usersDataSet1 usersDataSet1;
        private System.Windows.Forms.BindingSource auditsBindingSource;
        private usersDataSet1TableAdapters.auditsTableAdapter auditsTableAdapter;
        private System.Windows.Forms.TextBox bug_id_textbox;
        private System.Windows.Forms.TextBox audit_id_textbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn audit_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn bug_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn a_comments;
        private System.Windows.Forms.DataGridViewTextBoxColumn a_timestamp;
        private System.Windows.Forms.DataGridViewTextBoxColumn a_update;
    }
}