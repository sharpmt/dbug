﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
// -- Tutorial Links --


namespace DBug
{
    public partial class Form2 : Form
    {
        SqlConnection connection;
        private Users user;

        /// <summary>
        /// Class is accepting the user data so that it can determine which account is logged into the form. This is used so that user
        /// information is collected for revision notes, login information and when exporting the file it assignes their name within the fileName
        /// </summary>
        /// <param name="user"></param>
        public Form2(Users user)
        {
            this.user = user;
            InitializeComponent();
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionSrc"].ConnectionString);
            Label_User.Text = user.Username;

        }

        public Form2()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Nothing major, this class closes the forms and terminates the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBox1_Click(object sender, EventArgs e)
        {
            connection.Close();
            Application.Exit();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            
            // Checks to see if user wants to log out prior to logging out with a standard notification
            DialogResult result = MessageBox.Show("Are you sure you want to log out?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                connection.Close();
                Hide();
                Form1 LoginForm = new Form1();
                LoginForm.Show();
            }
            
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void PictureBox3_Click(object sender, EventArgs e)
        {
            // Reset GUI
            panel1.Visible = false;
            panel2.Visible = false;
            panel1.Location = new Point(484, 5);
            button3.Location = new Point(522, 384);
            button4.Location = new Point(583, 384);
            button5.Location = new Point(461, 384);
            button5.Visible = false;
            pictureBox7.Enabled = true;

            //Close any already open forms


            // Show graphic objs
            panel2.Visible = true;
            pictureBox6.Visible = true;
            textBox20.Visible = true;
            pictureBox3.Visible = true;
            splitter2.Size = new Size(387, 430);
            label3.Visible = true;
            ShowData();
            dataGridView1.Show();
            
        }
        // Fills DataTable for resuts
        private void ShowData()
        {
            // Using stored procedure to view data
            connection.Open();
            SqlCommand cmd = new SqlCommand("DisplayBugs", connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            dataGridView1.DataSource = dt;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            // Reset GUI
            splitter2.Visible = false;
            panel1.Visible = false;
            panel2.Visible = false;

            //Check if any files are already open
            textBox8.ResetText();
            textBox11.ResetText();
            textBox13.ResetText();
            textBox14.ResetText();
            textBox15.ResetText();
            textBox16.ResetText();
            textBox17.ResetText();
            textBox18.ResetText();
            textBox19.ResetText();
            textBox21.ResetText();
            radioButton1.Visible = true;
            radioButton2.Visible = true;
            BugID.ResetText();
            tb_submitted_on.ResetText();


            // Relocate the Panel for better UI
            panel1.Location = new Point(110, 0);
            button5.Location = new Point(583, 384);
            // Make relocated items visible
            panel1.Visible = true;
            label31.Visible = true;
            label4.Visible = true;
            textBox21.Visible = false;
            button5.Visible = true;
            button3.Visible = false;

            // Disable n/a items
            button1.Visible = false;
            button2.Visible = false;
            textBox13.Text = "0";
            textBox13.Enabled = false;

            tb_author.Enabled = false;
            tb_submitted_on.Enabled = false;
            textBox3.Enabled = false;
            tb_user_id.Enabled = false;
            textBox5.Enabled = false;
            textBox6.Enabled = false;
            textBox7.Enabled = false;
            textBox8.Enabled = false;
            textBox5.ResetText();
            textBox6.ResetText();
            textBox7.ResetText();
            textBox8.ResetText();

            comboBox1.Enabled = false;

            //Pre-fill data
            comboBox1.Text = "New";
            textBox9.Enabled = false;
            textBox9.Text = "New";
            textBox10.Enabled = false;
            DateTime CurrentTime = DateTime.Now;

            tb_author.Text = user.Username;
            tb_user_id.Text = user.User_id.ToString();

            // Sets the CurrentCulture property to U.S. English.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            textBox10.Text = CurrentTime.ToString();
            textBox12.Text = "0";
            textBox12.Enabled = false;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

            // Checks if cell data is existant + filled

            if (dataGridView1.CurrentRow.Cells[0].Value != DBNull.Value)
            {
                DataGridViewRow row = dataGridView1.CurrentRow;

                //Start populating 'Submission Details' text boxes
                tb_author.Text = row.Cells["b_author"].Value.ToString();
                tb_user_id.Text = row.Cells["b_user_id"].Value.ToString();
                tb_submitted_on.Text = row.Cells["b_timestamp"].Value.ToString();
                BugID.Text = row.Cells["bug_id"].Value.ToString();

                //Start populating 'Bug Details' text boxes
                textBox9.Text = row.Cells["b_status"].Value.ToString();
                textBox8.Text = row.Cells["b_name"].Value.ToString();
                comboBox1.Text = row.Cells["b_status"].Value.ToString();
                textBox10.Text = row.Cells["b_timestamp"].Value.ToString();
                textBox11.Text = row.Cells["b_desc"].Value.ToString();
                textBox12.Text = row.Cells["b_priority"].Value.ToString();
                textBox13.Text = row.Cells["b_revisions"].Value.ToString();
                textBox14.Text = row.Cells["b_application"].Value.ToString();
                textBox15.Text = row.Cells["b_line_s"].Value.ToString();
                textBox16.Text = row.Cells["b_line_f"].Value.ToString();
                textBox17.Text = row.Cells["b_line_no"].Value.ToString();
                textBox18.Text = row.Cells["b_class"].Value.ToString();
                textBox19.Text = row.Cells["b_method"].Value.ToString();
                textBox21.Text = row.Cells["b_source"].Value.ToString();
                tb_repo_uri.Text = row.Cells["b_uri"].Value.ToString();
                tb_proj_name.Text = row.Cells["b_projectName"].Value.ToString();
                tb_repo_name.Text = row.Cells["b_repoName"].Value.ToString();
                tb_file_name.Text = row.Cells["b_fileName"].Value.ToString();

                // Reset GUI for each data selection
                radioButton1.Visible = true;
                radioButton2.Visible = true;
                panel3.Visible = false;
                textBox5.Text = user.Username;
                textBox5.Enabled = false;
                textBox6.Text = user.User_id.ToString();
                textBox6.Enabled = false;
                textBox7.Text = row.Cells["b_revisions"].Value.ToString();
                textBox7.Enabled = false;
                textBox3.Enabled = true;
                button1.Enabled = false;
                button1.Visible = true;
                button2.Visible = true;
                textBox21.Visible = false;
                
                comboBox1.Enabled = true;
                textBox9.Enabled = false;
        
                if(comboBox1.Text == "Closed")
                {
                    panel1.Enabled = false;
                    textBox3.Text = "This bug has been closed and can no longer be modified. Should you need this bug, please re-open it manually.";
                    // MODIFY THIS WHEN FOREING KEY IS IMPLEMEMNTED
                    textBox3.ResetText();
                }
                else
                {
                    panel1.Enabled = true;
                }
                if(textBox13.Text == "0")
                {
                    button2.Enabled = false;
                    button2.Text = "Bug has 0 revisions";
                }
                else
                {
                    button2.Enabled = true;
                    button2.Text = "View Audit History";
                }
                if (textBox21.Text.Equals("") && tb_repo_uri.Text.Equals(""))
                {
                    button1.Enabled = false;
                    button1.Text = "Bug has no source";
                }
                else
                {
                    button1.Enabled = true;
                    button1.Text = "View Source Code";
                }


                connection.Close();

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DateTime CurrentTime = DateTime.Now;
            // Sets the CurrentCulture property to U.S. English.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");

            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();

                    SqlCommand sqlCmd = new SqlCommand("NewBug", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@bug_id", "");
                    sqlCmd.Parameters.AddWithValue("@b_name", textBox8.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_author", tb_author.Text);
                    sqlCmd.Parameters.AddWithValue("@b_user_id", tb_user_id.Text);
                    sqlCmd.Parameters.AddWithValue("@b_status", comboBox1.Text);
                    sqlCmd.Parameters.AddWithValue("@b_timestamp", CurrentTime);
                    sqlCmd.Parameters.AddWithValue("@b_revisions", textBox13.Text);
                    sqlCmd.Parameters.AddWithValue("@b_desc", textBox11.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_priority", textBox12.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_application", textBox14.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_lines", textBox15.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_linef", textBox16.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_lineno", textBox17.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_class", textBox18.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_method", textBox19.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_source", textBox21.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_projectName", tb_proj_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_repoName", tb_repo_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_fileName", tb_file_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_uri", tb_repo_uri.Text.Trim());
                    sqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Bug has been successfully submitted!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connection.Close();

                    //Reset Radio Buttons
                    radioButton1.Checked = false;
                    radioButton2.Checked = false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // This line of code loads data into the 'bugsTable.bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter1.Fill(this.bugsTable.bugs);
            // This line of code loads data into the 'usersDataSet.bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter.Fill(this.usersDataSet.bugs);
            int uid = user.Role;
            if (user.Role == 0)
            {
                pictureBox5.Location = new Point(19, 36);
                pictureBox3.Visible = false;
                pictureBox7.Visible = false;
                pictureBox8.Visible = false;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Shows panel
            label4.Visible = true;
            panel1.Visible = true;

            //Buttons
            button3.Visible = true;
            button4.Visible = true;

        }

        private void SearchData()
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();

                // Use DataAdapter to fill search results
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("DataSearch", connection);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@search", textBox20.Text);
                DataTable data = new DataTable();
                sqlDataAdapter.Fill(data);
                dataGridView1.DataSource = data;
                connection.Close();
                //ShowData();
            }
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            try
            {
                SearchData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime CurrentTime = DateTime.Now;
                // Sets the CurrentCulture property to U.K. English.
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                //Declaring int for ++ to add value by one incrementally 
                int ConvertedRevision;

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();

                    SqlCommand sqlCmd = new SqlCommand("EditBug", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    // Declare all variables in relation to UI fields given
                    sqlCmd.Parameters.AddWithValue("@bug_id", BugID.Text);
                    sqlCmd.Parameters.AddWithValue("@b_name", textBox8.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_author", tb_author.Text);
                    sqlCmd.Parameters.AddWithValue("@b_user_id", tb_user_id.Text);
                    sqlCmd.Parameters.AddWithValue("@b_status", comboBox1.Text);
                    sqlCmd.Parameters.AddWithValue("@b_timestamp", CurrentTime);
                    sqlCmd.Parameters.AddWithValue("@b_revisions", ConvertedRevision = Convert.ToInt32(textBox13.Text) + 1);
                    sqlCmd.Parameters.AddWithValue("@b_desc", textBox11.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_priority", textBox12.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_application", textBox14.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_lines", textBox15.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_linef", textBox16.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_lineno", textBox17.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_class", textBox18.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_method", textBox19.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_source", textBox21.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_projectName", tb_proj_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_repoName", tb_repo_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_fileName", tb_file_name.Text.Trim());
                    sqlCmd.Parameters.AddWithValue("@b_uri", tb_repo_uri.Text.Trim());
                    sqlCmd.ExecuteNonQuery();

                    //Update audit trail
                    SqlCommand UpAudit = new SqlCommand("UpdateAudit", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    UpAudit.Parameters.AddWithValue("@bug_id", BugID.Text);
                    UpAudit.Parameters.AddWithValue("@a_comments", textBox3.Text.Trim());
                    // Even though time stamp has already been pre-filled by the method DateTime() I have applied it again in the
                    // stored procedure so that if a dev opens a bug, it will re-update because they may not revise the bug until shortly after
                    UpAudit.Parameters.AddWithValue("@a_timestamp", CurrentTime);
                    UpAudit.Parameters.AddWithValue("@a_update", "Data has been changed, will interactively tell user");
                    UpAudit.Parameters.AddWithValue("@a_modified_by", tb_user_id.Text);
                    UpAudit.ExecuteNonQuery();

                    MessageBox.Show("Bug has been successfully revised!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    connection.Close();
                    ShowData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();

                    SqlCommand sqlCmd = new SqlCommand("DeleteData", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCmd.Parameters.AddWithValue("@bug_id", BugID.Text);
                    sqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Bug has been successfully Deleted!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    connection.Close();
                    ShowData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// BugID is being passed trough this class so that Form3 can use to to identify the Audits related to the Bug by using a foreign Key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(BugID.Text);
            form3.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string httpLink = textBox21.Text;
                string c_projectName = tb_proj_name.Text;
                string c_repoName = tb_repo_name.Text;
                string c_fileName = tb_file_name.Text;
                // URI is the URL link requires for source control
                GetSrcCode gsc = new GetSrcCode(new Uri(tb_repo_uri.Text.ToString()), user.PAT);
                Form4 form4 = new Form4();
                if (radioButton1.Checked)
                {
                    form4.Data = gsc.GetData(httpLink);
                }
                else if (radioButton2.Checked)/*User selects to retrieve via VSSC (Visual Studio Source Control - And provides URI PROJ REPO PATH */
                {
                    form4.Data = gsc.GetData(c_projectName, c_repoName, c_fileName);
                }
                form4.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Sorry, but the link you have provided is either an incorrect format or corrupt." + Environment.NewLine + Environment.NewLine + ex.Message, "Error");
            }
        }

        #region save file for export
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = "";
            saveFileDialog1.Title = "Export table as Excel File"; //Can I add PDF too?
            saveFileDialog1.FileName = "Bugs_Assigned_To_" + user.Username;
            saveFileDialog1.Filter = "Excel Workbook|*.xlsx";
            if (saveFileDialog1.ShowDialog() != DialogResult.Cancel)
            {
                Microsoft.Office.Interop.Excel.Application ExcelExport = new Microsoft.Office.Interop.Excel.Application();
                ExcelExport.Application.Workbooks.Add(Type.Missing);
                //Modify excel column size
                ExcelExport.Columns.ColumnWidth = 20;
                //Store header value
                for (int i = 1; i < dataGridView1.Columns.Count + 1; i++)
                {
                    ExcelExport.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;
                }
                //Store column & row value
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < dataGridView1.Columns.Count - 1; j++)
                    {
                        ExcelExport.Cells[i + 2, j + 1] = dataGridView1.Rows[i].Cells[j].Value.ToString();
                    }
                }
                //Save copy of workbook and close
                ExcelExport.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName.ToString());
                ExcelExport.ActiveWorkbook.Saved = true;
                ExcelExport.Quit();

            }
        }
        #endregion
        // The following classes are purely for GUI interface and looks
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.Visible = false;
            radioButton2.Visible = false;
            panel3.Visible = false;
            textBox21.Show();
            label35.Visible = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.Visible = false;
            radioButton2.Visible = false;
            panel3.Visible = true;
            label36.Visible = true;
            label35.Visible = false;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Form5 settings = new Form5(user);
            settings.Show();
        }

        private void label35_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
            textBox21.Visible = false;
            radioButton1.Visible = true;
            radioButton2.Visible = true;
            label35.Visible = false;

        }
        private void label36_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
            textBox21.Visible = false;
            radioButton1.Visible = true;
            radioButton2.Visible = true;
            label36.Visible = false;
        }

        private void pictureBox4_MouseEnter(object sender, EventArgs e)
        {
            label7.Visible = true;
        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {
            label7.Visible = false;
        }

        // Populates bug status
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            textBox9.Text = comboBox1.Text;
        }

        private void Label6_Click(object sender, EventArgs e)
        {
            splitter3.Size = new Size(785, 608);
            label4.Visible = true;
        }


        private void pictureBox5_MouseEnter(object sender, EventArgs e)
        {
            label14.Visible = true;
        }

        private void pictureBox5_MouseLeave(object sender, EventArgs e)
        {
            label14.Visible = false;
        }

        private void pictureBox3_MouseEnter(object sender, EventArgs e)
        {
            label15.Visible = true;
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            label15.Visible = false;
        }

        private void pictureBox7_MouseEnter(object sender, EventArgs e)
        {
            label37.Visible = true;
        }

        private void pictureBox7_MouseLeave(object sender, EventArgs e)
        {
            label37.Visible = false;
        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }
    }
}

// PRODUCTION NOTES


// TUTORIAL VIDEOS
