﻿using EasyScintilla.Stylers;

namespace DBug
{
    partial class Form4
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            EasyScintilla.Stylers.CSharpStyler cSharpStyler1 = new EasyScintilla.Stylers.CSharpStyler();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.simpleEditor1 = new EasyScintilla.SimpleEditor();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // simpleEditor1
            // 
            this.simpleEditor1.AutomaticFold = ((ScintillaNET.AutomaticFold)(((ScintillaNET.AutomaticFold.Show | ScintillaNET.AutomaticFold.Click)
            | ScintillaNET.AutomaticFold.Change)));
            this.simpleEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.simpleEditor1.IndentationGuides = ScintillaNET.IndentView.LookBoth;
            this.simpleEditor1.Lexer = ScintillaNET.Lexer.Cpp;
            this.simpleEditor1.Location = new System.Drawing.Point(0, 0);
            this.simpleEditor1.Name = "simpleEditor1";
            this.simpleEditor1.Size = new System.Drawing.Size(744, 501);
            cSharpStyler1.AutoIndent = true;
            cSharpStyler1.BraceMatching = true;
            cSharpStyler1.CodeFolding = true;
            cSharpStyler1.Lexer = ScintillaNET.Lexer.Cpp;
            cSharpStyler1.ShowLineNumbers = true;
            this.simpleEditor1.Styler = cSharpStyler1;
            this.simpleEditor1.TabIndex = 0;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(385, 144);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(250, 250);
            this.webBrowser1.TabIndex = 1;
            this.webBrowser1.Url = new System.Uri("", System.UriKind.Relative);
            this.webBrowser1.Visible = false;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 501);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.simpleEditor1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bug Source";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private EasyScintilla.SimpleEditor simpleEditor1;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}