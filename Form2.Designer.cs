﻿namespace DBug
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.tb_author = new System.Windows.Forms.TextBox();
            this.label_author = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_submitted_on = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_user_id = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_file_name = new System.Windows.Forms.TextBox();
            this.tb_repo_name = new System.Windows.Forms.TextBox();
            this.tb_proj_name = new System.Windows.Forms.TextBox();
            this.tb_repo_uri = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.BugID = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Label_User = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bug_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_user_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_revisions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_priority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_application = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_line_s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_line_f = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_line_no = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_method = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_audit_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_projectName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_repoName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_fileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.b_uri = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.bugsTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bugsTable = new DBug.BugsTable();
            this.bugsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersDataSet = new DBug.usersDataSet();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.bugsTableAdapter = new DBug.usersDataSetTableAdapters.bugsTableAdapter();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.bugsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bugsTableAdapter1 = new DBug.BugsTableTableAdapters.bugsTableAdapter();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label37 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1095, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 24);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(100, 419);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            this.splitter1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitter1_SplitterMoved);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.splitter2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter2.Enabled = false;
            this.splitter2.Location = new System.Drawing.Point(100, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(10, 419);
            this.splitter2.TabIndex = 5;
            this.splitter2.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1042, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(47, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.PictureBox2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(1046, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Logout";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Actions";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 7);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 24);
            this.label3.TabIndex = 9;
            this.label3.Text = "Assignments";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Data";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(1092, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Close";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(19, 36);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(57, 56);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.PictureBox3_Click);
            this.pictureBox3.MouseEnter += new System.EventHandler(this.pictureBox3_MouseEnter);
            this.pictureBox3.MouseLeave += new System.EventHandler(this.pictureBox3_MouseLeave);
            // 
            // splitter3
            // 
            this.splitter3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitter3.Location = new System.Drawing.Point(110, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(10, 419);
            this.splitter3.TabIndex = 14;
            this.splitter3.TabStop = false;
            // 
            // tb_author
            // 
            this.tb_author.Location = new System.Drawing.Point(23, 72);
            this.tb_author.Name = "tb_author";
            this.tb_author.ReadOnly = true;
            this.tb_author.Size = new System.Drawing.Size(137, 20);
            this.tb_author.TabIndex = 17;
            // 
            // label_author
            // 
            this.label_author.AutoSize = true;
            this.label_author.Location = new System.Drawing.Point(20, 56);
            this.label_author.Name = "label_author";
            this.label_author.Size = new System.Drawing.Size(38, 13);
            this.label_author.TabIndex = 18;
            this.label_author.Text = "Author";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Submitted on";
            // 
            // tb_submitted_on
            // 
            this.tb_submitted_on.Location = new System.Drawing.Point(23, 115);
            this.tb_submitted_on.Name = "tb_submitted_on";
            this.tb_submitted_on.ReadOnly = true;
            this.tb_submitted_on.Size = new System.Drawing.Size(177, 20);
            this.tb_submitted_on.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(157, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "User ID";
            // 
            // tb_user_id
            // 
            this.tb_user_id.Location = new System.Drawing.Point(160, 72);
            this.tb_user_id.Name = "tb_user_id";
            this.tb_user_id.ReadOnly = true;
            this.tb_user_id.Size = new System.Drawing.Size(40, 20);
            this.tb_user_id.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Reviewed by";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(23, 189);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(137, 20);
            this.textBox5.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(157, 173);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "User ID";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(160, 189);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(40, 20);
            this.textBox6.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(7, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "Add Comment";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Location = new System.Drawing.Point(13, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 111);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Submission Details";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Location = new System.Drawing.Point(13, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(197, 255);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Revision Notes";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(7, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(104, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "Number of Rivisions:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(7, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Status";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(117, 63);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(70, 20);
            this.textBox7.TabIndex = 43;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "New",
            "Viewed",
            "In Progress",
            "Awaiting More Information",
            "Awaiting Author",
            "Awaiting Developer",
            "Duplicate",
            "Closed",
            "Re-Opened"});
            this.comboBox1.Location = new System.Drawing.Point(10, 221);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(177, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(10, 102);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(177, 93);
            this.textBox3.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox4);
            this.panel3.Location = new System.Drawing.Point(201, 126);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(215, 220);
            this.panel3.TabIndex = 48;
            this.panel3.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.tb_file_name);
            this.groupBox4.Controls.Add(this.tb_repo_name);
            this.groupBox4.Controls.Add(this.tb_proj_name);
            this.groupBox4.Controls.Add(this.tb_repo_uri);
            this.groupBox4.Location = new System.Drawing.Point(3, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(209, 208);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Repository Details";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label36.Location = new System.Drawing.Point(103, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(101, 13);
            this.label36.TabIndex = 52;
            this.label36.Text = "- Change selection -";
            this.label36.Visible = false;
            this.label36.Click += new System.EventHandler(this.label36_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label34.Location = new System.Drawing.Point(7, 164);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(54, 13);
            this.label34.TabIndex = 52;
            this.label34.Text = "File Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label33.Location = new System.Drawing.Point(6, 116);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(64, 13);
            this.label33.TabIndex = 51;
            this.label33.Text = "Repo Name";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label32.Location = new System.Drawing.Point(6, 69);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(71, 13);
            this.label32.TabIndex = 50;
            this.label32.Text = "Project Name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(4, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 13);
            this.label17.TabIndex = 46;
            this.label17.Text = "Repo URI";
            // 
            // tb_file_name
            // 
            this.tb_file_name.Location = new System.Drawing.Point(7, 180);
            this.tb_file_name.Name = "tb_file_name";
            this.tb_file_name.Size = new System.Drawing.Size(196, 20);
            this.tb_file_name.TabIndex = 49;
            // 
            // tb_repo_name
            // 
            this.tb_repo_name.Location = new System.Drawing.Point(6, 132);
            this.tb_repo_name.Name = "tb_repo_name";
            this.tb_repo_name.Size = new System.Drawing.Size(196, 20);
            this.tb_repo_name.TabIndex = 48;
            // 
            // tb_proj_name
            // 
            this.tb_proj_name.Location = new System.Drawing.Point(7, 85);
            this.tb_proj_name.Name = "tb_proj_name";
            this.tb_proj_name.Size = new System.Drawing.Size(196, 20);
            this.tb_proj_name.TabIndex = 47;
            // 
            // tb_repo_uri
            // 
            this.tb_repo_uri.Location = new System.Drawing.Point(7, 39);
            this.tb_repo_uri.Name = "tb_repo_uri";
            this.tb_repo_uri.Size = new System.Drawing.Size(196, 20);
            this.tb_repo_uri.TabIndex = 46;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(19, 98);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(57, 56);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 33;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            this.pictureBox5.MouseEnter += new System.EventHandler(this.pictureBox5_MouseEnter);
            this.pictureBox5.MouseLeave += new System.EventHandler(this.pictureBox5_MouseLeave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.MenuText;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(82, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 18);
            this.label14.TabIndex = 34;
            this.label14.Text = "Submit a new bug";
            this.label14.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.MenuText;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(82, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 18);
            this.label15.TabIndex = 35;
            this.label15.Text = "View current bugs";
            this.label15.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.panel3);
            this.groupBox3.Controls.Add(this.BugID);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.textBox19);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.textBox17);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.textBox15);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.textBox14);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.textBox13);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.textBox12);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.textBox11);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.textBox10);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Location = new System.Drawing.Point(216, 35);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(422, 343);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bug Details";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label35.Location = new System.Drawing.Point(303, 299);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(101, 13);
            this.label35.TabIndex = 51;
            this.label35.Text = "- Change selection -";
            this.label35.Visible = false;
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton2.Location = new System.Drawing.Point(333, 316);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(74, 17);
            this.radioButton2.TabIndex = 50;
            this.radioButton2.Text = "VSS Code";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.radioButton1.Location = new System.Drawing.Point(212, 316);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(79, 17);
            this.radioButton1.TabIndex = 49;
            this.radioButton1.Text = "RAW Code";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // BugID
            // 
            this.BugID.Location = new System.Drawing.Point(83, -4);
            this.BugID.Name = "BugID";
            this.BugID.Size = new System.Drawing.Size(21, 20);
            this.BugID.TabIndex = 45;
            this.BugID.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label31.Location = new System.Drawing.Point(209, 299);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(69, 13);
            this.label31.TabIndex = 44;
            this.label31.Text = "Source Code";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label30.Location = new System.Drawing.Point(208, 251);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 13);
            this.label30.TabIndex = 42;
            this.label30.Text = "Method";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(211, 267);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(196, 20);
            this.textBox19.TabIndex = 41;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label29.Location = new System.Drawing.Point(6, 299);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 13);
            this.label29.TabIndex = 40;
            this.label29.Text = "Class";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(9, 315);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(196, 20);
            this.textBox18.TabIndex = 39;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Location = new System.Drawing.Point(6, 251);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(63, 13);
            this.label28.TabIndex = 38;
            this.label28.Text = "Error on line";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(9, 267);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(196, 20);
            this.textBox17.TabIndex = 37;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Location = new System.Drawing.Point(208, 203);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 13);
            this.label27.TabIndex = 36;
            this.label27.Text = "Line End";
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(211, 219);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(196, 20);
            this.textBox16.TabIndex = 35;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Location = new System.Drawing.Point(6, 203);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 13);
            this.label26.TabIndex = 34;
            this.label26.Text = "Line Start";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(9, 219);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(196, 20);
            this.textBox15.TabIndex = 33;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Location = new System.Drawing.Point(6, 157);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "Application";
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(9, 173);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(196, 20);
            this.textBox14.TabIndex = 31;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Location = new System.Drawing.Point(208, 157);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 13);
            this.label24.TabIndex = 30;
            this.label24.Text = "Bug Revision";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(211, 173);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(196, 20);
            this.textBox13.TabIndex = 29;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Location = new System.Drawing.Point(6, 110);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 28;
            this.label23.Text = "Bug Priority";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(9, 126);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(196, 20);
            this.textBox12.TabIndex = 27;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Location = new System.Drawing.Point(208, 64);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "Bug Description";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(211, 80);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(196, 66);
            this.textBox11.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Location = new System.Drawing.Point(6, 64);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(80, 13);
            this.label21.TabIndex = 24;
            this.label21.Text = "Bug Timestamp";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(9, 80);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(196, 20);
            this.textBox10.TabIndex = 23;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Location = new System.Drawing.Point(208, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Bug Status";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(211, 37);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(196, 20);
            this.textBox9.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(6, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 20;
            this.label19.Text = "Bug Name";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(9, 37);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(196, 20);
            this.textBox8.TabIndex = 19;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(211, 315);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(196, 20);
            this.textBox21.TabIndex = 43;
            this.textBox21.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label6.Location = new System.Drawing.Point(13, 380);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Logged in as:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label16.Location = new System.Drawing.Point(13, 398);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 39;
            // 
            // Label_User
            // 
            this.Label_User.AutoSize = true;
            this.Label_User.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Label_User.Location = new System.Drawing.Point(13, 398);
            this.Label_User.Name = "Label_User";
            this.Label_User.Size = new System.Drawing.Size(0, 13);
            this.Label_User.TabIndex = 40;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(216, 384);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 23);
            this.button2.TabIndex = 41;
            this.button2.Text = "View Audit History";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(337, 384);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 23);
            this.button1.TabIndex = 42;
            this.button1.Text = "View Source Code";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.tb_author);
            this.panel1.Controls.Add(this.label_author);
            this.panel1.Controls.Add(this.tb_submitted_on);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.tb_user_id);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBox6);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Location = new System.Drawing.Point(484, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(646, 414);
            this.panel1.TabIndex = 43;
            this.panel1.Visible = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Lime;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(461, 384);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(55, 23);
            this.button5.TabIndex = 45;
            this.button5.Text = "Submit";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(583, 384);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(55, 23);
            this.button4.TabIndex = 44;
            this.button4.Text = "Delete";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.OrangeRed;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(522, 384);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 23);
            this.button3.TabIndex = 43;
            this.button3.Text = "Revise";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bug_id,
            this.b_name,
            this.b_author,
            this.b_user_id,
            this.b_status,
            this.b_timestamp,
            this.b_revisions,
            this.b_desc,
            this.b_priority,
            this.b_application,
            this.b_line_s,
            this.b_line_f,
            this.b_line_no,
            this.b_class,
            this.b_method,
            this.b_source,
            this.b_audit_id,
            this.b_projectName,
            this.b_repoName,
            this.b_fileName,
            this.b_uri});
            this.dataGridView1.DataSource = this.bugsBindingSource2;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView1.Location = new System.Drawing.Point(6, 77);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(363, 334);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // bug_id
            // 
            this.bug_id.DataPropertyName = "bug_id";
            this.bug_id.HeaderText = "bug_id";
            this.bug_id.Name = "bug_id";
            this.bug_id.ReadOnly = true;
            this.bug_id.Visible = false;
            // 
            // b_name
            // 
            this.b_name.DataPropertyName = "b_name";
            this.b_name.HeaderText = "Name";
            this.b_name.Name = "b_name";
            this.b_name.ReadOnly = true;
            // 
            // b_author
            // 
            this.b_author.DataPropertyName = "b_author";
            this.b_author.HeaderText = "b_author";
            this.b_author.Name = "b_author";
            this.b_author.ReadOnly = true;
            this.b_author.Visible = false;
            // 
            // b_user_id
            // 
            this.b_user_id.DataPropertyName = "b_user_id";
            this.b_user_id.HeaderText = "b_user_id";
            this.b_user_id.Name = "b_user_id";
            this.b_user_id.ReadOnly = true;
            this.b_user_id.Visible = false;
            // 
            // b_status
            // 
            this.b_status.DataPropertyName = "b_status";
            this.b_status.HeaderText = "Status";
            this.b_status.Name = "b_status";
            this.b_status.ReadOnly = true;
            // 
            // b_timestamp
            // 
            this.b_timestamp.DataPropertyName = "b_timestamp";
            this.b_timestamp.HeaderText = "Submitted On";
            this.b_timestamp.Name = "b_timestamp";
            this.b_timestamp.ReadOnly = true;
            // 
            // b_revisions
            // 
            this.b_revisions.DataPropertyName = "b_revisions";
            this.b_revisions.HeaderText = "b_revisions";
            this.b_revisions.Name = "b_revisions";
            this.b_revisions.ReadOnly = true;
            this.b_revisions.Visible = false;
            // 
            // b_desc
            // 
            this.b_desc.DataPropertyName = "b_desc";
            this.b_desc.HeaderText = "Description";
            this.b_desc.Name = "b_desc";
            this.b_desc.ReadOnly = true;
            // 
            // b_priority
            // 
            this.b_priority.DataPropertyName = "b_priority";
            this.b_priority.HeaderText = "Priority";
            this.b_priority.Name = "b_priority";
            this.b_priority.ReadOnly = true;
            // 
            // b_application
            // 
            this.b_application.DataPropertyName = "b_application";
            this.b_application.HeaderText = "b_application";
            this.b_application.Name = "b_application";
            this.b_application.ReadOnly = true;
            this.b_application.Visible = false;
            // 
            // b_line_s
            // 
            this.b_line_s.DataPropertyName = "b_lines";
            this.b_line_s.HeaderText = "b_lines";
            this.b_line_s.Name = "b_line_s";
            this.b_line_s.ReadOnly = true;
            this.b_line_s.Visible = false;
            // 
            // b_line_f
            // 
            this.b_line_f.DataPropertyName = "b_linef";
            this.b_line_f.HeaderText = "b_linef";
            this.b_line_f.Name = "b_line_f";
            this.b_line_f.ReadOnly = true;
            this.b_line_f.Visible = false;
            // 
            // b_line_no
            // 
            this.b_line_no.DataPropertyName = "b_lineno";
            this.b_line_no.HeaderText = "b_lineno";
            this.b_line_no.Name = "b_line_no";
            this.b_line_no.ReadOnly = true;
            this.b_line_no.Visible = false;
            // 
            // b_class
            // 
            this.b_class.DataPropertyName = "b_class";
            this.b_class.HeaderText = "b_class";
            this.b_class.Name = "b_class";
            this.b_class.ReadOnly = true;
            this.b_class.Visible = false;
            // 
            // b_method
            // 
            this.b_method.DataPropertyName = "b_method";
            this.b_method.HeaderText = "b_method";
            this.b_method.Name = "b_method";
            this.b_method.ReadOnly = true;
            this.b_method.Visible = false;
            // 
            // b_source
            // 
            this.b_source.DataPropertyName = "b_source";
            this.b_source.HeaderText = "b_source";
            this.b_source.Name = "b_source";
            this.b_source.ReadOnly = true;
            this.b_source.Visible = false;
            // 
            // b_audit_id
            // 
            this.b_audit_id.DataPropertyName = "b_audit_id";
            this.b_audit_id.HeaderText = "b_audit_id";
            this.b_audit_id.Name = "b_audit_id";
            this.b_audit_id.ReadOnly = true;
            this.b_audit_id.Visible = false;
            // 
            // b_projectName
            // 
            this.b_projectName.DataPropertyName = "b_projectName";
            this.b_projectName.HeaderText = "b_projectName";
            this.b_projectName.Name = "b_projectName";
            this.b_projectName.ReadOnly = true;
            this.b_projectName.Visible = false;
            // 
            // b_repoName
            // 
            this.b_repoName.DataPropertyName = "b_repoName";
            this.b_repoName.HeaderText = "b_repoName";
            this.b_repoName.Name = "b_repoName";
            this.b_repoName.ReadOnly = true;
            this.b_repoName.Visible = false;
            // 
            // b_fileName
            // 
            this.b_fileName.DataPropertyName = "b_fileName";
            this.b_fileName.HeaderText = "b_fileName";
            this.b_fileName.Name = "b_fileName";
            this.b_fileName.ReadOnly = true;
            this.b_fileName.Visible = false;
            // 
            // b_uri
            // 
            this.b_uri.DataPropertyName = "b_uri";
            this.b_uri.HeaderText = "b_uri";
            this.b_uri.Name = "b_uri";
            this.b_uri.ReadOnly = true;
            this.b_uri.Visible = false;
            // 
            // bugsBindingSource2
            // 
            this.bugsBindingSource2.DataMember = "bugs";
            this.bugsBindingSource2.DataSource = this.bugsTableBindingSource;
            // 
            // bugsTableBindingSource
            // 
            this.bugsTableBindingSource.DataSource = this.bugsTable;
            this.bugsTableBindingSource.Position = 0;
            // 
            // bugsTable
            // 
            this.bugsTable.DataSetName = "BugsTable";
            this.bugsTable.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bugsBindingSource
            // 
            this.bugsBindingSource.DataMember = "bugs";
            this.bugsBindingSource.DataSource = this.usersDataSet;
            // 
            // usersDataSet
            // 
            this.usersDataSet.DataSetName = "usersDataSet";
            this.usersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textBox20
            // 
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.Location = new System.Drawing.Point(6, 47);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(334, 24);
            this.textBox20.TabIndex = 44;
            this.textBox20.Visible = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(346, 47);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(23, 24);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 45;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.pictureBox6);
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.textBox20);
            this.panel2.Location = new System.Drawing.Point(101, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(372, 414);
            this.panel2.TabIndex = 46;
            this.panel2.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.MenuText;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(85, 309);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 18);
            this.label7.TabIndex = 48;
            this.label7.Text = "Settings";
            this.label7.Visible = false;
            // 
            // bugsTableAdapter
            // 
            this.bugsTableAdapter.ClearBeforeFill = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(19, 291);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(57, 56);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 47;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            this.pictureBox4.MouseEnter += new System.EventHandler(this.pictureBox4_MouseEnter);
            this.pictureBox4.MouseLeave += new System.EventHandler(this.pictureBox4_MouseLeave);
            // 
            // bugsBindingSource1
            // 
            this.bugsBindingSource1.DataMember = "bugs";
            this.bugsBindingSource1.DataSource = this.bugsTable;
            // 
            // bugsTableAdapter1
            // 
            this.bugsTableAdapter1.ClearBeforeFill = true;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox7.Enabled = false;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(19, 194);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(57, 56);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 49;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
            this.pictureBox7.MouseEnter += new System.EventHandler(this.pictureBox7_MouseEnter);
            this.pictureBox7.MouseLeave += new System.EventHandler(this.pictureBox7_MouseLeave);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(12, 273);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(75, 12);
            this.pictureBox9.TabIndex = 51;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(12, 174);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(75, 12);
            this.pictureBox8.TabIndex = 52;
            this.pictureBox8.TabStop = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.SystemColors.MenuText;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label37.Location = new System.Drawing.Point(83, 214);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(51, 18);
            this.label37.TabIndex = 53;
            this.label37.Text = "Export";
            this.label37.Visible = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1128, 419);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Label_User);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.splitter3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tb_author;
        private System.Windows.Forms.Label label_author;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_submitted_on;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_user_id;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label Label_User;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panel2;
        private usersDataSet usersDataSet;
        private System.Windows.Forms.BindingSource bugsBindingSource;
        private usersDataSetTableAdapters.bugsTableAdapter bugsTableAdapter;
        private System.Windows.Forms.TextBox BugID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tb_repo_name;
        private System.Windows.Forms.TextBox tb_proj_name;
        private System.Windows.Forms.TextBox tb_repo_uri;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tb_file_name;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private BugsTable bugsTable;
        private System.Windows.Forms.BindingSource bugsBindingSource1;
        private BugsTableTableAdapters.bugsTableAdapter bugsTableAdapter1;
        private System.Windows.Forms.BindingSource bugsBindingSource2;
        private System.Windows.Forms.BindingSource bugsTableBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn bug_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_author;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_user_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_timestamp;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_revisions;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_priority;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_application;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_line_s;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_line_f;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_line_no;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_class;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_method;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_source;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_audit_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_projectName;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_repoName;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_fileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn b_uri;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}