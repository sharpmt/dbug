﻿using System;
using System.Windows.Forms;

namespace DBug
{
    public partial class Form4 : Form
    {
        public string Data { get; set; }
        /// <summary>
        /// Form4 constructor
        /// </summary>
        /// <param name="code">Pulls across the data from form2</param>
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            simpleEditor1.Text = Data;
            // Text editor = data property
        }
    }
}

// TUTORIALS
// https://docs.microsoft.com/en-us/vsts/integrate/concepts/dotnet-client-libraries - .NET client libraries for VSTS (and TFS)
// https://www.visualstudio.com/en-us/docs/integrate/api/overview - REST API Overview
// https://docs.microsoft.com/en-gb/rest/api/vsts/ - REST API



// GENERATED PATS

//              rfi4m6fdh5pfbl5muvkfmdloayqrkj7bvxtf52xfxcivxkrnwmqq - FULL ACCESS              

// Use the #region tag to define what area is used for what
// Use automatic commenting through XML schema ("///") to be used as a blurb