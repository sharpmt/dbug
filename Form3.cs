﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBug
{
    public partial class Form3 : Form
    {
        // Generates SQL connection to access bug table data
        SqlConnection connection;

        #region
        /// <summary>
        /// This class performs the connection from form to the bug data table. Form3 the receives the string bug_id through
        /// Form2 so that it can accurately find the right audits for the given bug.
        /// </summary>
        /// <param name="bug_id"></param>
        public Form3(int bug_id)
        {
            InitializeComponent();
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionSrc"].ConnectionString);
            bug_id_textbox.Text = bug_id.ToString();
        }
        #endregion
        private void Form3_Load(object sender, EventArgs e)
        {
            // This line of code loads data into the 'usersDataSet1.audits' table. You can move, or remove it, as needed.
            auditsTableAdapter.Fill(this.usersDataSet1.audits);
     
            // try:catch statement checks first to see if there is a connection already made with the database and if not
            // ensures that one is open so it can connect allowing a stored procedure to execute
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();

                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("ShowAudit", connection);
                    sqlDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    sqlDataAdapter.SelectCommand.Parameters.AddWithValue("@bug_id", bug_id_textbox.Text);
                    DataTable data = new DataTable();
                    sqlDataAdapter.Fill(data);
                    dataGridView1.DataSource = data;
                    //MessageBox.Show("Audit trail successfully loaded!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        #region
        /// <summary>
        /// This class waits for a selection changed within the datagridview. This is so that when the audit changes, the submission details
        /// change so that live update occurs on Form2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[0].Value != DBNull.Value)
            {
                DataGridViewRow row = dataGridView1.CurrentRow;

                //Start populating 'Submission Details' text boxes
                audit_id_textbox.Text = row.Cells["audit_id"].Value.ToString();
            }
        }
        #endregion
    }
}
