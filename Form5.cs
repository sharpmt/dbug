﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBug
{
    public partial class Form5 : Form
    {
        SqlConnection connection;
        private Users user;
        /// <summary>
        /// carrying the user data from the users table so that the user can apply a PAT to their account, that way they can be granted access for source control
        /// </summary>
        /// <param name="user">user class</param>
        public Form5(Users user)
        {
            InitializeComponent();
            this.user = user;
            textBox1.Text = user.PAT;
        }
        /// <summary>
        /// The button allows for the user to link at PAT to their user account for source control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionSrc"].ConnectionString);
            int user_id = user.User_id;
            
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();

                    SqlCommand sqlCmd = new SqlCommand("SetPAT", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    // Adds the parameter to the stored procedure and populates the value given
                    sqlCmd.Parameters.AddWithValue("@pat", textBox1.Text);
                    sqlCmd.Parameters.AddWithValue("@user_id", user_id);
                    sqlCmd.ExecuteNonQuery();
                    // Generic message relayed back to the user so that they can see when the button has been clicked
                    MessageBox.Show("PAT has been assigned! You will need to log out and back in to enable the token", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    connection.Close();
                }

                Hide();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}
