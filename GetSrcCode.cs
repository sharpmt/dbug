﻿using System;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using Microsoft.VisualStudio.Services.WebApi;
using System.IO;

namespace DBug
{
    public sealed class GetSrcCode
    {
        VssConnection vssconnection;

        public GetSrcCode(Uri Uri, string PATAuth)
        {
            vssconnection = new VssConnection(Uri, new VssBasicCredential(string.Empty, PATAuth));
        }

        public string GetData(string c_projectName, string c_repoName, string c_fileName = null)
        {
            try
            {
                // Get a GitHttpClient to talk to the Git endpoints
                GitHttpClient gitClient = vssconnection.GetClient<GitHttpClient>();

                StreamReader sr = new StreamReader(gitClient.GetItemContentAsync(c_projectName, c_repoName, c_fileName, null, null, null, null, false, null, null).Result);
                return sr.ReadToEnd();
            }
            catch (Exception)
            {
                Exception ex = new Exception("Failed to connect to the repo! Please ensure that your PAT code is assigned & authorised to view this VSTS repo. Alternatively, is this a valid VSTS?");
                throw new Exception(ex.Message);
            }

        }

        public string GetData(string HttpLink)
        {
            try
            {
                string webData = string.Empty;
                System.Net.WebClient wc = new System.Net.WebClient();
                webData = wc.DownloadString(HttpLink);
                return webData;
                
            }
            catch (Exception)
            {
                Exception ex = new Exception("Sorry, but the link you have provided is either invalid or corrupt.");
                throw new Exception(ex.Message);
            }
        }

    }
}