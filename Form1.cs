﻿using System;
using System.Windows.Forms;

namespace DBug
{

    public partial class Form1 : Form
    {
        
        private Users user;

        public Form1()
        {
            InitializeComponent();
            InitVariables();
        }

        private void InitVariables()
        {
            user = new Users();
        }
        private void LoginCommand()
        {
            try
            {
                user = user.GetUserRecord(textBox1.Text);

                //Checks to see if the username field is empty for validation, notifying the user that their data is incorrect
                if (user.Username.Equals(string.Empty))
                {
                    // Display error saying user doesn't exist.
                    label4.Visible = true;
                }
                else
                {
                    if (user.Password.Equals(textBox2.Text))
                    {
                        // Once the password is checked, the user can then progress to the next form. It is checked
                        // against the database instead of allowing for user input to prevent XSS scripting
                        user.Password = string.Empty;
                        Form2 form2 = new Form2(user);
                        form2.ShowDialog();
                        Hide();
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                string noAccount = ex.ToString();
                label4.Visible = true;
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
            
        }

        private void TextBox1_Enter(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // Form2 passes the data user through so that it can be used to access the username of the user and the relevant ID for further progression
            Form2 f2 = new Form2(user);
            f2.ShowDialog();
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            LoginCommand();               
        }
        /// <summary>
        /// Class awaits the key function enter to be pressed so that when doing so, the user can then run the LoginCommand which
        /// checks to see if their data is allowed for authentication. This is in place for a more user friendly UI.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoginCommand();
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}

// USEFUL INFORMATION
                        //PAT   - rfi4m6fdh5pfbl5muvkfmdloayqrkj7bvxtf52xfxcivxkrnwmqq
                        //VSTS  - https://sharpmt10.visualstudio.com/
