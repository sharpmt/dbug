﻿CREATE TABLE [dbo].[bugs] (
    [bug_id]        INT           IDENTITY (1, 1) NOT NULL,
    [b_name]        NVARCHAR (50) NULL,
    [b_author]      NVARCHAR (50) NOT NULL,
    [b_user_id]     INT           NOT NULL,
    [b_status]      NVARCHAR (50) NULL,
    [b_timestamp]   DATETIME      NULL,
    [b_revisions]   INT           NULL,
    [b_desc]        TEXT          NULL,
    [b_priority]    INT           NULL,
    [b_application] NVARCHAR (50) NOT NULL,
    [b_lines]       INT           NULL,
    [b_linef]       INT           NULL,
    [b_lineno]      INT           NULL,
    [b_class]       NVARCHAR (50) NULL,
    [b_method]      NVARCHAR (50) NULL,
    [b_source]      TEXT          NULL,
    PRIMARY KEY CLUSTERED ([bug_id] ASC),
    CONSTRAINT [FK_b_user_id] FOREIGN KEY ([b_user_id]) REFERENCES [dbo].[accounts] ([user_id])
);

